﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeGizmo : MonoBehaviour
{
    [SerializeField] Color color;

    void OnDrawGizmos ()
    {
        Gizmos.matrix = this.transform.localToWorldMatrix;
        Gizmos.color = color;
        Gizmos.DrawCube (Vector3.zero, Vector3.one);
        Gizmos.matrix = Matrix4x4.identity;
    }
}
